### This first block filters genetic data and extracts the features that will be used for model training (PCs and SNPs)
# First the initial VCF file is filtered based on MAF and LD. A GDS file is produced for next analysis
R < Scripts/filterVCF.R --no-save
# PCAir Script using the GDS file
R < Scripts/genoPCAir.R --no-save

### Next Block selects the 790 SNPs most relevant to the trait and saves them in csv. format
R < Scripts/FormatDataForCropGBM.R --no-save
# Prepare the genotype data for CropGBM and format it. (this loads the vcf file whihc requires alot of RAM, so the script makes an R dataframe of the useful data)

# now run cropGBM and find the most significant SNP
mkdir 02_Intermediate_Data/gbm_result/

cropgbm -o 02_Intermediate_Data/gbm_result/ -e -t -sf --bygain-boxplot --traingeno 02_Intermediate_Data/train.geno --trainphe 02_Intermediate_Data/train.phe --min-gain 0.01 --max-colorbar 0.6 --cv-times 10

# Extract the 785 most important SNPs 
R < Scripts/getSNP.R --no-save

### Next block processes the management and soil data
R < Scripts/ProcessManagementandSoil.R --no-save

### In the next block weather data is processed along with the other preprocessed inputs to generate numpy files which will constitute the input of the models
# First we infer the harvest date of 2022 based on previous data (variable used to define the days used as input for the weather variables)
R < Scripts/makeDATEHfile.R --no-save

# A list of Genotypes is generated to be used in the following scripts
cut -f 1 -d ',' 02_Intermediate_Data/SNP785.csv | grep -v hybrid > 02_Intermediate_Data/GENOlist.txt

# First training data is processed (Weather and previous) and saved in 03_Final_Data

python Scripts/ProcessTrainingData.py

# Same procedure is followed for testing data

python Scripts/ProcessTestingData.py

# Before starting the modelling, we perform an outlier detection

R < Scripts/OutlierDetect.R --no-save

### In the next block the model is built, trained and used for prediction of the test split (2022 data)

# Create a directory where checkpoints will be saved during training 
mkdir 04_Model_Output/intermediate_models

# Run the training. (It imports everything "*" from ModelPreprocess.py file)

python Scripts/TrainModel.py

# The model generated is used to predict 2022
python Scripts/Predict.py

# Metrics(MSE, RMSE, R2 and r) are computed per location and averaged.
python Scripts/ComputeMetrics.py
