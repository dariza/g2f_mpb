from numpy.random import seed

seed(1)

import numpy as np
from pandas import read_csv
from sklearn.preprocessing import MinMaxScaler

Tx = 30
n_var = 10
#val_size = 0.1
#test_size = 0.1
data_dir = "03_Processed_Data"
#data_predict = "data/predict"


# Import training data which we will use in its completeness to train the model
x_weather = np.load("%s/weather_train.npy" %(data_dir))    
x_data_pca = np.load("%s/pca_train.npy" %(data_dir))   
x_data_snp = np.load("%s/SNP785_train.npy" %(data_dir))   
x_data_soil_management = np.load("%s/soil_man_train.npy" %(data_dir))  
y_data = np.load("%s/yield_year_location_all_data.npy" %(data_dir),allow_pickle = True)   # (123026, 3) yield, year, location

# Filter outliers
outlier = read_csv("%s/outlierdetect.csv" %(data_dir))
outliers_tf = outlier["Outlier"]

# Remove outlier data with the TF vector stored before
x_weather_train = x_weather[~outliers_tf]   # (122844, 30, 10)
x_data_pca_train = x_data_pca[~outliers_tf]   # (122844, 27)
x_data_snp_train = x_data_snp[~outliers_tf]   # (122844, 790)
x_data_soil_management_train = x_data_soil_management[~outliers_tf]   # (122844, 20)
y_data_train = y_data[~outliers_tf]   # (122844, 3)

# Import prediction data
x_weather_pred = np.load("%s/weather_test.npy" %(data_dir))    # (11555, 30, 10)
x_data_pca_pred = np.load("%s/pca_test.npy" %(data_dir))   # (11555, 27)
x_data_snp_pred = np.load("%s/SNP785_test.npy" %(data_dir))   # (11555, 790)
x_data_soil_management_pred = np.load("%s/soil_man_test.npy" %(data_dir))    # (11555, 20)


# Scale features 
# Two separate scalers for X, Y (diff dimensions) 
scaler_x = MinMaxScaler(feature_range=(-1, 1))
scaler_y =  MinMaxScaler(feature_range=(-1, 1))

x_train_reshaped = x_weather_train.reshape((x_weather_train.shape[0], x_weather_train.shape[1] * x_weather_train.shape[2]))

yield_train = y_data_train [:, 0]   # (82692, )
yield_train = yield_train.reshape((yield_train.shape[0], 1))   # (82692, 1)

# Scaling Coefficients calculated from the training dataset
scaler_x = scaler_x.fit(x_train_reshaped)   
scaler_y = scaler_y.fit(yield_train)

# Function to scale features after fitting
def scale_features (data_x_train,data_x_pred, data_y):
    
    data_x_train = data_x_train.reshape((data_x_train.shape[0], data_x_train.shape[1] * data_x_train.shape[2]))
    data_x_train = scaler_x.transform(data_x_train)
    data_x_train = data_x_train.reshape((data_x_train.shape[0], Tx, n_var))

    data_x_pred = data_x_pred.reshape((data_x_pred.shape[0], data_x_pred.shape[1] * data_x_pred.shape[2]))
    data_x_pred = scaler_x.transform(data_x_pred)
    data_x_pred = data_x_pred.reshape((data_x_pred.shape[0], Tx, n_var))

    data_yield = data_y [:, 0]
    data_yield = data_yield.reshape((data_yield.shape[0], 1))
    data_yield = scaler_y.transform(data_yield)
    
    return data_x_train, data_x_pred, data_yield



# Scale features

x_weather_train, x_weather_pred, y_data_train = scale_features(x_weather_train, x_weather_pred, y_data_train)
