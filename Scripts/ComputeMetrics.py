# -*- coding: utf-8 -*-
import pandas as pd

import numpy as np
from sklearn.metrics import mean_squared_error, r2_score
from scipy.stats import pearsonr

predictions = pd.read_csv("04_Model_Output/prediction.csv")
predictions.rename(columns={'Yield_Mg_ha' : 'yield_predicted'}, inplace = True)
true_yields = pd.read_csv("01_Raw_Data/Testing_Data/Final_Observed_Yield.csv")
true_yields.rename(columns={'Yield_Mg_ha' : 'yield_true'}, inplace = True)
### There are 1265 less entries in the true yield than in the predictions

all_yields = true_yields.merge(predictions, on = ['Env', 'Hybrid'], how = 'left')


true = all_yields['yield_true'].values
predicted = all_yields['yield_predicted'].values

MSE = mean_squared_error(true, predicted)
RMSE = np.sqrt(MSE)
R2 = r2_score(true,predicted)
r = pearsonr(true,predicted)[0]

##### Statistics across locations
locations = np.unique(all_yields['Env'])
df = pd.DataFrame(locations, columns = ['Env'])
columns = ['Env', 'MSE', 'RMSE', 'R2', 'r']
MSE = []
R2 = []
r = []

for env in locations: 
    #Filter data for that env
    loc_mask = pd.array(all_yields['Env'] == env)
    loc_data = all_yields[loc_mask]
    loc_true = loc_data['yield_true'].values
    loc_pred = loc_data['yield_predicted'].values
    loc_MSE = mean_squared_error(loc_true,loc_pred)
    MSE.append(loc_MSE)
    loc_r2 = r2_score(loc_true,loc_pred)
    R2.append(loc_r2)
    loc_r = pearsonr(loc_true,loc_pred)[0]
    r.append(loc_r)
    
RMSE = np.sqrt(MSE)
df['MSE'] = MSE
df['RMSE'] = RMSE
df['R2'] = R2
df['r'] = r
means = df.mean(axis=0, numeric_only= True)
row = ['Means']
[row.append(x) for x in means.tolist()]
df.loc[len(df)] = row
df.to_csv("04_Model_Output/metrics_per_location.csv", index=False)
print("Mean RMSE: " + str(np.mean(RMSE)))
print("Mean R2:   " + str(np.mean(R2)))
print("Mean r:    " + str(np.mean(r)))

