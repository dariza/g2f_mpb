# Initializations
import os
os.environ['PYTHONHASHSEED']=str(0)
from numpy.random import seed 
seed(1)
import random
random.seed(1)
import tensorflow
tensorflow.random.set_seed(1)

from ModelPreprocess import *    # Check
from keras.models import load_model


os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = ""
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "1"
os.environ["TF_DETERMINISTIC_OPS"] = "1"
os.environ["TF_ENABLE_ONEDNN_OPTS"] = "0" 
os.environ["KERAS_BACKEND"] = "tensorflow"

import tensorflow as tf
config = tf.compat.v1.ConfigProto(intra_op_parallelism_threads=1,inter_op_parallelism_threads=1)
tf.random.set_seed(1)
sess = tf.compat.v1.Session(graph=tf.compat.v1.get_default_graph(), config=config)
from keras import backend as K
K.set_session(sess)

# Change datatype for the previously processed prediction inputs
x_weather_pred = x_weather_pred.astype('float32')
x_data_pca_pred = x_data_pca_pred.astype('float32')
x_data_snp_pred = x_data_snp_pred.astype('float32')
x_data_soil_management_pred = x_data_soil_management_pred.astype('float32')


## Load previously trained model
pred_model = load_model('04_Model_Output/model_final.h5')
# Make predictions
yield_data_hat = pred_model.predict([x_weather_pred, x_data_pca_pred, x_data_snp_pred, x_data_soil_management_pred], batch_size = 32)
# Transform to original yield units
yield_data_hat = scaler_y.inverse_transform(yield_data_hat)


## Load submission template
submission_temp = read_csv('01_Raw_Data/Testing_Data/1_Submission_Template_2022.csv')

# Replace last column with the prediction and save
submission_temp['Yield_Mg_ha'] = yield_data_hat

submission_temp.to_csv('04_Model_Output/prediction.csv', index = False)