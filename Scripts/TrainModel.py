# Initializations and imports of used packages
run_num = 1
import os
os.environ['PYTHONHASHSEED']=str(0)
from numpy.random import seed 
seed(1)
import random
random.seed(1)
import tensorflow
tensorflow.random.set_seed(1)


from keras.layers import Concatenate, Input, LSTM, Dense
from keras.optimizers import Adam
from keras.models import Model


# Set up to run on GPU
os.environ["CUDA_VISIBLE_DEVICES"] = "0"  
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "1"
os.environ["TF_DETERMINISTIC_OPS"] = "1"
os.environ["TF_ENABLE_ONEDNN_OPTS"] = "0" 
os.environ["KERAS_BACKEND"] = "tensorflow"
 
import tensorflow as tf
config = tf.compat.v1.ConfigProto(intra_op_parallelism_threads=1,inter_op_parallelism_threads=1)
tf.random.set_seed(1)
sess = tf.compat.v1.Session(graph=tf.compat.v1.get_default_graph(), config=config)
from keras import backend as K
K.set_session(sess)

# Importing the ready to use data that is processed in ModelPreprocess.py
from ModelPreprocess import *




# Directory to save Results
dir_save = '04_Model_Output/'

# set data type for training objects
x_weather_train = x_weather_train.astype('float32')  
x_data_pca_train = x_data_pca_train.astype('float32')   
x_data_snp_train = x_data_snp_train.astype('float32')   
x_data_soil_management_train = x_data_soil_management_train.astype('float32')  
y_data_train = y_data_train.astype('float32')   


# Number of Variables
weather_dim1 = x_weather_train.shape[1]
weather_dim2 = x_weather_train.shape[2]  
pca_dim = x_data_pca_train.shape[1]   
SNP_dim = x_data_snp_train.shape[1]
SoilMan_dim = x_data_soil_management_train.shape[1]

# Defining parameters for training
batch_size = 128
epochs = 100 
lr_rate = 0.000001   

concatenator = Concatenate(axis=-1)

# Model definition

def model():
    # Prepare inputs for the models based on the defined dimensions of the input 
    weather_input = Input(shape = (weather_dim1, weather_dim2))
    pca_input = Input(shape = (pca_dim, )) 
    snp_input = Input(shape = (SNP_dim, ))
    soilman_input = Input(shape = (SoilMan_dim,))
    
    # LSTM string that processes weather data      
    lstm, state_h, state_c = LSTM(256, return_state=True, return_sequences=True)(weather_input)
    lstm, state_h, state_c = LSTM(256, return_state=True, return_sequences=True)(lstm)        
    lstm, state_h, state_c = LSTM(128, return_state=True, return_sequences=False)(lstm)    
    lstm = Dense(128, activation = "relu")(lstm)    
    lstm = Dense(64, activation = "relu")(lstm)        
    lstm = Dense(32, activation = "relu")(lstm)        
    lstm = Dense(16, activation = "relu")(lstm)    
    lstm = Dense(8, activation = "relu")(lstm)
        
    # This stream processes PCA data sepparately    
    pca = Dense(64, activation = "relu")(pca_input)    
    pca = Dense(64, activation = "relu")(pca)     
    pca = Dense(32, activation = "relu")(pca)        
    pca = Dense(16, activation = "relu")(pca)
    pca = Dense(8, activation = "relu")(pca)
        
    
    # This stream processes the SNP data
    snp = Dense(790, activation = "relu")(snp_input)
    snp = Dense(790, activation = "relu")(snp)
    snp = Dense(512, activation = "relu")(snp)
    snp = Dense(256, activation = "relu")(snp)
    snp = Dense(128, activation = "relu")(snp)
    snp = Dense(64, activation = "relu")(snp)
    snp = Dense(32, activation = "relu")(snp)
    snp = Dense(16, activation = "relu")(snp) 
    snp = Dense(8, activation = "relu")(snp)
            
    # Both genetic data get concatenated in the next layer
    genome = concatenator([snp,pca])   
    genome = Dense(16, activation = "relu")(genome)

    # Stream that processes management and soil data sepparately
    soilman = Dense(64, activation = "relu")(soilman_input)
    soilman = Dense(64, activation = "relu")(soilman)
    soilman = Dense(32, activation = "relu")(soilman)
    soilman = Dense(16, activation = "relu")(soilman)
    soilman = Dense(16, activation = "relu")(soilman)


    # All streams get combined and processed for a few more layers
    combined = concatenator([lstm,genome, soilman])  
    combined = Dense(16*3, activation = "relu")(combined)
    combined = Dense(16*2, activation = "relu")(combined)
    combined = Dense(16, activation = "relu")(combined)
    combined = Dense(8, activation = "relu")(combined)

    # FC Layer
    yhat = Dense (1, activation = "tanh")(combined)   # (None, 1)
        
    pred_model = Model([weather_input, pca_input,snp_input, soilman_input], yhat)   # Prediction Model
        
    return pred_model
            
# Model initialization and Summary
pred_model = model()
pred_model.summary()

# Define a checkpoint callback so we save a model every 10 epochs
filepath = dir_save+ "/intermediate_models/saved-model-{epoch:03d}.h5"
checkpoint = tf.keras.callbacks.ModelCheckpoint(filepath, monitor='mean_squared_error', verbose=1, save_best_only=False, mode='max', period = 10)






# Model training
optimizer = Adam(learning_rate=lr_rate , decay = 0.0001)
pred_model.compile(loss='mean_squared_error', metrics = ['mean_squared_error'], optimizer = optimizer)  

hist = pred_model.fit ([x_weather_train, x_data_pca_train, x_data_snp_train, x_data_soil_management_train], y_data_train,
                  batch_size = batch_size,
                  epochs = epochs,
                  callbacks = [checkpoint],   
                  verbose = 1,
                  shuffle = False)

# Save the model 
pred_model.save(dir_save +  'model_final.h5')

