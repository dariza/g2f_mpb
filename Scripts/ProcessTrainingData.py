from pandas import read_csv
import pandas as pd
import numpy as np


pheno = read_csv("01_Raw_Data/Training_Data/1_Training_Trait_Data_2014_2021.csv")

# Formating dates for consistency
pheno["DATE"] = pd.to_datetime(pheno['Date_Planted'], infer_datetime_format=True)
pheno["DATEH"] = pd.to_datetime(pheno['Date_Harvested'], infer_datetime_format=True)

#Filtering training data based on Genotyped individuals
GID = read_csv("02_Intermediate_Data/GENOlist.txt",header=None)
GID['row_num'] = np.arange(len(GID))
GID = GID.rename(columns={0: "Hybrid"})
phenox = pd.merge(pheno,GID).reset_index()


pheno2 = phenox.dropna(how="any",subset=['DATEH']).reset_index()

# Loading weather training data and formating time for consistency
weather = read_csv("01_Raw_Data/Training_Data/4_Training_Weather_Data_2014_2021.csv")
weather['DATE'] = pd.to_datetime(weather['Date'].astype(str).str[0:4] + "-" + weather['Date'].astype(str).str[4:6] + "-" + weather['Date'].astype(str).str[6:8], infer_datetime_format=True)

# Filtering training data which has no weather recording
WENV = weather['Env'].unique()
pheno3 = pheno2[pheno2['Env'].isin(WENV)].reset_index(drop=True)

# Filtering training data based on missing values for the target variable (yield)
pheno4 = pheno3.dropna(subset=['Yield_Mg_ha']).reset_index(drop=True)

# Creating empty 3D array based on the number of samples, variables in weather and time_steps
n_samples = pheno4.shape[0]
n_periods = 30
n_variables = 10   

X_weekly = np.zeros((n_samples, n_periods, n_variables)) 




for x in range(n_samples):
	print(x)
	mask = pd.array(weather['Env'] == pheno4['Env'][x])
	df = weather[mask]
	m2 = pd.array(df['DATE'] < pheno4['DATEH'][x])
	dx = df[m2].tail(150)
	n_timesteps = dx.shape[0]   
	n_variables = 10  
	data_interval_1= 5
	nb_timesteps_1 = n_timesteps/ data_interval_1   
	nb_timesteps = int(nb_timesteps_1)
	dataset = dx.to_numpy()
	data_reshaped = np.zeros ((nb_timesteps, n_variables))   
	for i in range(0, nb_timesteps):
		range_1 = i * data_interval_1
		range_2 = (i + 1) * data_interval_1
		data = dataset [range_1:range_2, :]  
		data_reshaped[i, 0] = np.mean(data[:,2],axis=0) # QV2M
		data_reshaped[i, 1] = np.mean(data[:,3],axis=0) # T2MDEW
		data_reshaped[i, 2] = np.mean(data[:,4],axis=0) # PS
		data_reshaped[i, 3] = np.mean(data[:,5],axis=0) # RH2M
		data_reshaped[i, 4] = np.mean(data[:,6],axis=0) # WS2M
		data_reshaped[i, 5] = np.max(data[:,10],axis=0) # T2M_MAX
		data_reshaped[i, 6] = np.min(data[:,11],axis=0) # T2M_MIN
		data_reshaped[i, 7] = np.mean(data[:,12],axis=0) # T2MWET
		data_reshaped[i, 8] = np.mean(data[:,14],axis=0) # T2M
		data_reshaped[i, 9] = np.mean(data[:,17],axis=0) # PRECTOTCORR
		    
	X_weekly[x,:,:] = data_reshaped
	
data_dir_intermediate = "02_Intermediate_Data"            
data_dir_final = "03_Processed_Data"

# Save 3D weather array
np.save("%s/weather_train.npy"%(data_dir_final), X_weekly)

# Save a csv with the yield values (target variable to be predicted by the model) and some accessory data that will ease the manipulation
pheno5  = pheno4[["Yield_Mg_ha","Year","Field_Location"]].to_numpy()
np.save("%s/yield_year_location_all_data.npy" %(data_dir_final),pheno5)


# Save an csv file with useful data for outlier detection
pheno6  = pheno4[["Hybrid","Yield_Mg_ha","Year","Field_Location"]].to_numpy()
np.savetxt('%s/pheno6.csv'%(data_dir_intermediate), pheno6,delimiter=",",fmt='%s')


# And their genotypes only
genotypeID = pheno4['row_num'].to_numpy()
np.save("%s/genotype_id_all_data.npy" %(data_dir_intermediate),genotypeID)






##### In this section we load the previously generated csv files and process them to be npy objects
PCA = read_csv("02_Intermediate_Data/5_Genotype_Data_All_Years_maf2pct_ld975pct_PCAir1.csv")
PCA = PCA.rename(columns={PCA.columns[0]: "Hybrid"})

x_data_pca = np.zeros((pheno4.shape[0],PCA.shape[1]-2))

for i in range(0, genotypeID.shape[0]):
    x_data_pca[i,:] =  PCA[PCA["Hybrid"] == pheno4["Hybrid"][i]].to_numpy()[:,1:(PCA.shape[1]-1)]

np.save("%s/pca_train.npy" %(data_dir_final),x_data_pca)




#######################################################

SNP = read_csv("02_Intermediate_Data/SNP785.csv")

x_data_snp790 = np.zeros((pheno4.shape[0],SNP.shape[1]-1))

for i in range(0, pheno4.shape[0]):
     x_data_snp790[i,:] =  SNP[SNP["hybrid"] == pheno4["Hybrid"][i]].to_numpy()[:,1:]

np.save("%s/SNP785_train.npy" %(data_dir_final),x_data_snp790)

################################
# add the soil and management data for training

man_soil = read_csv("02_Intermediate_Data/Soil_Management_Train.csv")
x_data_management_soil = np.zeros((pheno4.shape[0], man_soil.shape[1]-1))
for i in range(0,pheno4.shape[0]):
    x_data_management_soil[i,:] = man_soil[man_soil["Env"] == pheno4["Env"][i]].to_numpy()[:,1:21]

np.save("%s/soil_man_train.npy" %(data_dir_final),x_data_management_soil)

