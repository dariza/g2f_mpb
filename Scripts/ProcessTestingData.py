from pandas import read_csv
import pandas as pd
import numpy as np

pheno = read_csv("01_Raw_Data/Testing_Data/1_Submission_Template_2022.csv")
weather = read_csv("01_Raw_Data/Testing_Data/4_Testing_Weather_Data_2022.csv")

weather['DATE'] = pd.to_datetime(weather['Date'].astype(str).str[0:4] + "-" + weather['Date'].astype(str).str[4:6] + "-" + weather['Date'].astype(str).str[6:8], infer_datetime_format=True)




dateh = read_csv("02_Intermediate_Data/DATEH2022.csv")
dateh['DATEH'] = pd.to_datetime(dateh['DATEH'], format="%d.%m.%Y")

pheno2 = pheno.merge(dateh)
n_samples = pheno2.shape[0]
n_periods = 30
n_variables = 10


X_weekly = np.zeros((n_samples, n_periods, n_variables)) 

for x in range(n_samples):
	mask = pd.array(weather['Env'] == pheno2['Env'][x])
	df = weather[mask]
	m2 = pd.array(df['DATE'] < pheno2['DATEH'][x])
	dx = df[m2].tail(150)
	n_timesteps = dx.shape[0]   
	n_variables = 10    
	data_interval_1= 5
	nb_timesteps_1 = n_timesteps/ data_interval_1   
	nb_timesteps = int(nb_timesteps_1)
	dataset = dx.to_numpy()
	data_reshaped = np.zeros ((nb_timesteps, n_variables))   
	for i in range(0, nb_timesteps):
		range_1 = i * data_interval_1
		range_2 = (i + 1) * data_interval_1
		data = dataset [range_1:range_2, :] 
		data_reshaped[i, 0] = np.mean(data[:,2],axis=0) # QV2M
		data_reshaped[i, 1] = np.mean(data[:,10],axis=0) # T2MDEW
		data_reshaped[i, 2] = np.mean(data[:,3],axis=0) # PS
		data_reshaped[i, 3] = np.mean(data[:,6],axis=0) # RH2M
		data_reshaped[i, 4] = np.mean(data[:,4],axis=0) # WS2M
		data_reshaped[i, 5] = np.max(data[:,9],axis=0) # T2M_MAX
		data_reshaped[i, 6] = np.min(data[:,14],axis=0) # T2M_MIN
		data_reshaped[i, 7] = np.mean(data[:,8],axis=0) # T2MWET
		data_reshaped[i, 8] = np.mean(data[:,13],axis=0) # T2M
		data_reshaped[i, 9] = np.mean(data[:,16],axis=0) # PRECTOTCORR
		    
	X_weekly[x,:,:] = data_reshaped

data_dir_final = "03_Processed_Data"

# Save 3D weather array
np.save("%s/weather_test.npy"%(data_dir_final), X_weekly)





PCA = read_csv("02_Intermediate_Data/5_Genotype_Data_All_Years_maf2pct_ld975pct_PCAir1.csv")
PCA = PCA.rename(columns={PCA.columns[0]: "Hybrid"})

x_data_pca = np.zeros((pheno2.shape[0],PCA.shape[1]-2))

for i in range(0,pheno2.shape[0]):
    x_data_pca[i,:] =  PCA[PCA["Hybrid"] == pheno2["Hybrid"][i]].to_numpy()[:,1:(PCA.shape[1]-1)]

np.save("%s/pca_test.npy"%(data_dir_final),x_data_pca)




###################
SNP = read_csv("02_Intermediate_Data/SNP785.csv")

x_data_snp = np.zeros((pheno2.shape[0],SNP.shape[1]-1))

for i in range(0, pheno2.shape[0]):
     x_data_snp[i,:] =  SNP[SNP["hybrid"] == pheno2["Hybrid"][i]].to_numpy()[:,1:]

np.save("%s/SNP785_test.npy"%(data_dir_final),x_data_snp)


# add the soil and management data for training for testing/prediction
man_soil = read_csv("02_Intermediate_Data/Soil_Management_Test.csv")
x_data_management_soil = np.zeros((pheno2.shape[0], man_soil.shape[1]-1))
for i in range(0,pheno2.shape[0]):
    x_data_management_soil[i,:] = man_soil[man_soil["Env"] == pheno2["Env"][i]].to_numpy()[:,1:21]

np.save("%s/soil_man_test.npy"%(data_dir_final),x_data_management_soil)


