# MaizeG2F participation of the MPB_group

In this repository you can find all the scripts, initial, intermediate and final files to reproduce our participation in the Genomes to Fields (G2F) Genotype by Environment Prediction Competition. The structure of the repository is described in detail below. 

## environment.yml

In this file you can find the output of running the following command in the conda environment used to develop and run all the code

```
conda env export > environment.yml
``` 

You can recreate the enviornment by running: 

```
conda env create -f environment.yml
``` 

## run.sh

1-click file that will run the whole pipeline from the initial data to the output. Before every action, you will find comments that are helpful to understand the pipeline. 

## 01_Raw_Data

In this directory you will find the data as it was provided to us. Same as found in:  https://doi.org/10.25739/tq5e-ak26

## 02_Intermediate_Data

In this directory you will find all the intermediate files that are produced during the run of the pipeline but that will not be a direct input to the model. 

## 03_Processed_Data

Here you will find ready to use ".npy" and ".csv" files. This constitute the direct input for the model. 

## 04_Model_Output

Here you will find the final trained model as well as some checkpoints (every 10 epochs). Also here you will find the predictions and the computed metrics per environment as well as the mean of all environments. 



